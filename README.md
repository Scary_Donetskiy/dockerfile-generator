# Dockerfile Generator #

This small tool may help you with creating Dockerfile

### Requirements ###

* Python >= 3.5.2
* NodeJS >= 7.10.1

### How do I get set up? ###

* Install Python dependencies. Also you can use [virtualenv](http://docs.python-guide.org/en/latest/dev/virtualenvs/)
```bash
pip install -r requirements.txt
```
* Install Node dependencies and build frontend application
```bash
cd src/frontend
npm run build
```
* Run application
```bash
cd src
python main.py
```

### Run tests ###

* Run tests for ProxyServer (Python)
```bash
cd src
py.test -s test.py
```

### Contribution guidelines ###

* Writing tests
* Code review
* Guidelines accord