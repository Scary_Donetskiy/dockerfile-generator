#!/usr/local/bin/python3
# -*- coding: utf-8 -*-


class UrlProvider:
    @staticmethod
    def get_docker_images_url(query, page):
        return 'https://hub.docker.com/v2/search/repositories/?page=' + str(page) + '&query=' + query

    @staticmethod
    def get_image_info_url(image):
        if len(image.split('/')) != 1:
            return 'https://hub.docker.com/v2/repositories/' + image
        else:
            return 'https://hub.docker.com/v2/repositories/library/' + image
