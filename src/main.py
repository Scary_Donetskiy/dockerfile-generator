#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
import os

import cherrypy
import requests

from url_provider import UrlProvider


class ProxyServer(object):
    @cherrypy.expose
    def index(self):
        return open('frontend/index.html')

    @cherrypy.expose
    def get_images(self, query='', page=1):
        cherrypy.log('get_images')
        request = requests.Session()
        result = request.get(UrlProvider.get_docker_images_url(query, page))
        return result.text

    @cherrypy.expose
    def get_info(self, image):
        cherrypy.log('get_info')
        request = requests.Session()
        result = request.get(UrlProvider.get_image_info_url(image))
        return result.text

    @cherrypy.expose
    def proxy_query(self, query):
        cherrypy.log('proxy_query')
        cherrypy.log(query)
        request = requests.Session()
        result = request.get(query)
        return result.text

    @cherrypy.expose
    def download(self, dockerfile):
        cherrypy.response.headers['Content-Description'] = 'File Transfer'
        cherrypy.response.headers['Content-Type'] = 'text/plain'
        cherrypy.response.headers['Content-Disposition'] = 'attachment; filename="Dockerfile"'
        cherrypy.response.headers['Expires'] = '0'
        cherrypy.response.headers['Cache-Control'] = 'must-revalidate'
        cherrypy.response.headers['Pragma'] = 'public'
        cherrypy.response.headers['Content-Length'] = len(dockerfile)
        return dockerfile


if __name__ == '__main__':
    cherrypy.config.update('config/server.conf')
    cherrypy.quickstart(ProxyServer(), '/', 'config/app.conf')
