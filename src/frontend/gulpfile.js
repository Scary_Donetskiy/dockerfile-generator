let gulp = require('gulp');
let browserify = require('browserify');
let babelify = require('babelify');
let source = require('vinyl-source-stream');

gulp.task('build', () => {
    return browserify({
        entries: './src/app.jsx',
        extensions: ['.jsx'],
        debug: true
    }).transform('babelify', {
        presets: ['es2015', 'react']
    }).bundle()
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('dist'));
});

gulp.task('copy-vendor', () => {
    return gulp.src([
        'node_modules/imperavi-kube/dist/css/kube.css',
        'node_modules/imperavi-kube/dist/js/kube.js',
        'node_modules/jquery/dist/jquery.js',
        'node_modules/codemirror/lib/codemirror.css'
    ])
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', ['build'], () => {
    gulp.watch('./src/**/*.jsx', ['build']);
});

gulp.task('default', ['watch']);
