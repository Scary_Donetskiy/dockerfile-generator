import React from 'react';
import EventEmitterFactory from '../../factories/EventEmitterFactory';
import {CHANGE_PAGE} from '../../factories/EventEmitterFactory';

const RESULTS_PER_PAGE = 20;

/**
 * Pagination component
 */
export default class Pagination extends React.Component {
    /**
     * @param {{imagesCount: number}} props
     */
    constructor(props) {
        super(props);
        this.changePage = this.changePage.bind(this);
        this.eventEmitter = EventEmitterFactory.getEmitter();
        this.pageCount = this.props.imagesCount / RESULTS_PER_PAGE;
        this.currentPage = 1;
    }

    /**
     * @param nextProps
     */
    componentWillReceiveProps(nextProps) {
        this.pageCount = nextProps.imagesCount / RESULTS_PER_PAGE;
    }

    /**
     * @param {Event} e
     */
    changePage(e) {
        e.preventDefault();
        switch (e.target.dataset.direction) {
            case 'prev':
                this.currentPage--;
                break;
            case 'next':
                this.currentPage++;
                break;
        }
        this.eventEmitter.emit(CHANGE_PAGE, this.currentPage);
    }

    /**
     * @returns {XML}
     */
    render() {
        let prevLink = this.currentPage !== 1 ?
            <li className="prev"><a data-direction="prev" onClick={this.changePage}>&lt; Prev</a></li> : null;
        let nextLink = this.currentPage !== this.pageCount ?
            <li className="next"><a data-direction="next" onClick={this.changePage}>Next &gt;</a></li> : null;

        return this.props.imagesCount !== 0 && this.pageCount !== 0 ? (
            <nav className="pagination align-center upper strong">
                <ul>
                    {prevLink}
                    {nextLink}
                </ul>
            </nav>
        ) : null;
    }
}