import React from 'react';
import _ from 'underscore';
import EventEmitterFactory from '../../factories/EventEmitterFactory';
import {SEARCH_RESULTS} from '../../factories/EventEmitterFactory';
import ModelsFactory from '../../factories/ModelsFactory';
import Pagination from '../Pagination/Pagination';
import ImageSmallDescription from "../ImageSmallDescription/ImageSmallDescription";

/**
 * List of search results
 */
export default class SearchResults extends React.Component {
    /**
     * @param props
     */
    constructor(props) {
        super(props);
        this.eventEmitter = EventEmitterFactory.getEmitter();
        this.state = {
            searchResults: ModelsFactory.getSearchResults()
        };
    }

    componentDidMount() {
        this.eventEmitter.addListener(SEARCH_RESULTS, (data) => {
            this.setState({
                searchResults: data
            });
        });
    }

    componentWillUnmount() {
        this.eventEmitter.removeListener(SEARCH_RESULTS);
    }

    /**
     * Render component
     * @returns {XML}
     */
    render() {
        let images = _.map(this.state.searchResults.results, (item) => {
            return <ImageSmallDescription data={item} key={_.uniqueId()}/>;
        });

        return (
            <div>
                <p className="smaller">Found images: {this.state.searchResults.count}</p>
                {images}
                <Pagination imagesCount={this.state.searchResults.count}/>
            </div>
        );
    }
}