import React from 'react';
import UrlProvider from '../../providers/UrlProvider';
import SearchResults from '../SearchResults/SearchResults';
import EventEmitterFactory from '../../factories/EventEmitterFactory';
import {SEARCH_RESULTS, CHANGE_PAGE} from '../../factories/EventEmitterFactory';

/**
 * Image search block component
 */
export default class ImageSearch extends React.Component {
    /**
     * @param props
     */
    constructor(props) {
        super(props);
        this.searchClick = this.searchClick.bind(this);
        this.gotDataCallback = this.gotDataCallback.bind(this);
        this.eventEmitter = EventEmitterFactory.getEmitter();
    }

    /**
     * Search Docker images by query
     */
    searchClick() {
        let query = this.imageNameInput.value;
        fetch(UrlProvider.getImages(query)).then(this.gotDataCallback);
    }

    /**
     * @param {*} response
     */
    gotDataCallback(response) {
        response.json().then((data) => {
            this.eventEmitter.emit(SEARCH_RESULTS, data);
        });
    }

    componentDidMount() {
        this.eventEmitter.addListener(CHANGE_PAGE, (page) => {
            fetch(UrlProvider.getImages(this.imageNameInput.value, page)).then(this.gotDataCallback);
        })
    }

    /**
     * @returns {XML}
     */
    render() {
        return (
            <fieldset>
                <legend>Search Docker image</legend>
                <div className="form-item">
                    <label>Type image name:</label>
                    <div className="append">
                        <input type="text" placeholder="Search" ref={(input) => {
                            this.imageNameInput = input;
                        }}/>
                        <button className="button outline" onClick={this.searchClick}>Search</button>
                    </div>
                </div>
                <SearchResults/>
            </fieldset>
        );
    }
}