import React from 'react';
import ReactDOM from 'react-dom';
import UrlProvider from '../../providers/UrlProvider';
import ImageDetailsModal from '../ImageDetailsModal/ImageDetailsModal';
import EventEmitterFactory from '../../factories/EventEmitterFactory';
import {SELECT_IMAGE} from '../../factories/EventEmitterFactory';

/**
 * List of images item component
 */
export default class ImageSmallDescription extends React.Component {
    /**
     * @param props
     */
    constructor(props) {
        super(props);
        this.detailsClick = this.detailsClick.bind(this);
        this.selectClick = this.selectClick.bind(this);
        this.eventEmitter = EventEmitterFactory.getEmitter();
    }

    /**
     * Show modal window with image details
     */
    detailsClick() {
        fetch(UrlProvider.getInfo(this.props.data.repo_name)).then((response) => {
            response.json().then((data) => {
                ReactDOM.render(<ImageDetailsModal data={data}/>, this.modalDiv);
            });
        });
    }

    /**
     * Select image as base for Dockerfile
     */
    selectClick() {
        this.eventEmitter.emit(SELECT_IMAGE, this.props.data);
    }

    /**
     * @returns {XML}
     */
    render() {
        let official = this.props.data.is_official ?
            <span>Official <i className="material-icons md-18">check circle</i></span> : null;
        return (
            <fieldset>
                <legend>{this.props.data.repo_name}</legend>
                <p className="smaller">{official}{this.props.data.star_count} stars</p>
                <p className="small">{this.props.data.short_description}</p>
                <div className="group">
                    <button className="button outline small float-left" onClick={this.detailsClick}>Details</button>
                    <button className="button small float-right" onClick={this.selectClick}>Select image</button>
                </div>
                <div ref={(div) => {
                    this.modalDiv = div;
                }}>{/* Place for modal component */}</div>
            </fieldset>
        );
    }
}