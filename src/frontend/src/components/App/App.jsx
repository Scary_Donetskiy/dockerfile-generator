import React from 'react';
import ImageSearch from './../ImageSearch/ImageSearch';
import Parameters from "../Parameters/Parameters";

/**
 * Application main component
 */
export default class App extends React.Component {
    /**
     * Render main component
     * @returns {XML}
     */
    render() {
        return (
            <div className="app" style={{padding: '20px'}}>
                <h5 className="title">Dockerfile Generator</h5>
                <div className="row">
                    <div className="col col-4">
                        <ImageSearch/>
                    </div>
                    <div className="col col-8">
                        <Parameters/>
                    </div>
                </div>
                <p>Created by <a href="http://vados.pro" target="_blank">vados.pro</a> | <a
                    href="https://bitbucket.org/Scary_Donetskiy/dockerfile-generator" target="_blank">Source code</a> |
                    <a href="https://bitbucket.org/Scary_Donetskiy/" target="_blank">Bitbucket</a></p>
            </div>
        );
    }
}