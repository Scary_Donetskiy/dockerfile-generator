import React from 'react';
import _ from 'underscore';
import markdown from 'markdown';

/**
 * Modal window with image detail description
 */
export default class ImageDetailsModal extends React.Component {
    /**
     * @param props
     */
    constructor(props) {
        super(props);
        this.modalUniqueId = _.uniqueId('image-details-modal');
        this.description = {__html: 'Description is not exist :('};
        if (this.props.data.full_description !== null) {
            this.description = this.props.data.full_description.replace('↵', "\n");
            this.description = {__html: markdown.markdown.toHTML(this.description)};
        }
    }

    componentDidMount() {
        $.modalwindow({
            target: `#${this.modalUniqueId}`,
            width: 'auto'
        })
    }

    /**
     * @returns {XML}
     */
    render() {
        return (
            <div id={this.modalUniqueId} className="modal-box hide">
                <div className="modal">
                    <span className="close"/>
                    <div className="modal-header">{this.props.data.name}</div>
                    <div className="modal-body" dangerouslySetInnerHTML={this.description}/>
                </div>
            </div>
        );
    }
}