import React from 'react';
import EventEmitterFactory from "../../factories/EventEmitterFactory";
import {SELECT_IMAGE, GENERATE} from "../../factories/EventEmitterFactory";
import Generator from "../Generator/Generator";

/**
 * Component with Dockerfile parameters
 */
export default class Parameters extends React.Component {
    /**
     * @param props
     */
    constructor(props) {
        super(props);
        this.eventEmitter = EventEmitterFactory.getEmitter();
        this.generateClick = this.generateClick.bind(this);
    }

    componentDidMount() {
        this.eventEmitter.addListener(SELECT_IMAGE, (data) => {
            this.baseImageInput.value = data.repo_name;
        });
    }

    /**
     * Emit event to generate Dockerfile
     */
    generateClick() {
        this.eventEmitter.emit(GENERATE, {
            from: this.baseImageInput.value,
            version: this.versionImageInput.value,
            maintainer: this.maintainerInput.value,
            run: this.runInput.value,
            cmd: this.cmdInput.value,
            expose: this.exposePortsInput.value,
            env: this.envInput.value,
            add: this.addInput.value,
            copy: this.copyInput.value,
            volume: this.volumeInput.value,
            workdir: this.workDirInput.value
        });
    }

    /**
     * @returns {XML}
     */
    render() {
        return (
            <fieldset>
                <legend>Dockerfile Parameters</legend>
                <div className="row gutters">
                    <div className="col col-6">
                        <div className="form-item">
                            <div className="prepend">
                                <span>FROM</span>
                                <input type="text" ref={(input) => {
                                    this.baseImageInput = input;
                                }} defaultValue="ubuntu"/>
                            </div>
                            <div className="desc">Sets the base image for subsequent instructions. Use search form for
                                select main image
                            </div>
                        </div>
                    </div>
                    <div className="col col-6">
                        <div className="form-item">
                            <div className="prepend">
                                <span>IMAGE VERSION</span>
                                <input type="text" ref={(input) => {
                                    this.versionImageInput = input;
                                }} defaultValue="latest"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="form-item">
                    <div className="prepend">
                        <span>MAINTAINER</span>
                        <input type="text" ref={(input) => {
                            this.maintainerInput = input;
                        }} defaultValue="Dockerfile Generator"/>
                    </div>
                    <div className="desc">Sets the author field of the generated image
                    </div>
                </div>

                <div className="form-item">
                    <div className="prepend">
                        <span>RUN</span>
                        <textarea cols="30" rows="3" ref={(area) => {
                            this.runInput = area;
                        }} placeholder="<command>"/>
                    </div>
                    <div className="desc">Executes any commands in a new layer on top of the current image and commit
                        the results
                    </div>
                </div>

                <div className="form-item">
                    <div className="prepend">
                        <span>CMD</span>
                        <input type="text" ref={(area) => {
                            this.cmdInput = area;
                        }} placeholder="[&quot;executable&quot;,&quot;param1&quot;,&quot;param2&quot;]"/>
                    </div>
                    <div className="desc">Sets the command to be executed when running the image</div>
                </div>

                <div className="form-item">
                    <div className="prepend">
                        <span>EXPOSE</span>
                        <input type="text" ref={(input) => {
                            this.exposePortsInput = input;
                        }} placeholder="<port> <port>" defaultValue="80 443"/>
                    </div>
                    <div className="desc">Informs Docker that the container listens on the specified network ports at
                        runtime. Separate ports by spaces
                    </div>
                </div>

                <div className="form-item">
                    <div className="prepend">
                        <span>ENV</span>
                        <textarea cols="30" rows="3" ref={(area) => {
                            this.envInput = area;
                        }} placeholder="<key> <value>"/>
                    </div>
                    <div className="desc">Sets the environment variables</div>
                </div>

                <div className="form-item">
                    <div className="prepend">
                        <span>ADD</span>
                        <textarea cols="30" rows="3" ref={(area) => {
                            this.addInput = area;
                        }} placeholder="<src> <dest>"/>
                    </div>
                    <div className="desc">Copies new files, directories or remote file URLs</div>
                </div>

                <div className="form-item">
                    <div className="prepend">
                        <span>COPY</span>
                        <textarea cols="30" rows="3" ref={(area) => {
                            this.copyInput = area;
                        }} placeholder="<src> <dest>"/>
                    </div>
                    <div className="desc">Copies new files or directories</div>
                </div>

                <div className="form-item">
                    <div className="prepend">
                        <span>VOLUME</span>
                        <input type="text" ref={(input) => {
                            this.volumeInput = input;
                        }} placeholder="/data,/data2"/>
                    </div>
                    <div className="desc">Creates a mount point with the specified name and marks it as holding
                        externally mounted volumes from native host or other containers. Separate by commas
                    </div>
                </div>

                <div className="form-item">
                    <div className="prepend">
                        <span>WORKDIR</span>
                        <input type="text" ref={(input) => {
                            this.workDirInput = input;
                        }} placeholder="/path/to/workdir"/>
                    </div>
                    <div className="desc">Sets the working directory for any RUN, CMD, ENTRYPOINT, COPY and ADD
                        instructions that follow it in the Dockerfile
                    </div>
                </div>

                <button className="button outline small" onClick={this.generateClick}>Generate</button>
                <Generator/>
            </fieldset>
        );
    }
}