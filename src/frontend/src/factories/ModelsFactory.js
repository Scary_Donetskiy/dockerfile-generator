/**
 * Factory with empty models structures
 */
export default class ModelsFactory {
    /**
     * @returns {{count: number, next: null, previous: null, results: Array}}
     */
    static getSearchResults() {
        return {
            count: 0,
            next: null,
            previous: null,
            results: []
        };
    }
}