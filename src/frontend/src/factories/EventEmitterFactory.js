import EventEmitter from "wolfy87-eventemitter";

export const SEARCH_RESULTS = 'SearchResults';
export const SELECT_IMAGE = 'SelectImage';
export const CHANGE_PAGE = 'ChangePage';
export const GENERATE = 'Generate';

/**
 * Factory for EventEmitter
 */
export default class EventEmitterFactory {
    /**
     * @returns {EventEmitter}
     */
    static getEmitter() {
        if (this.emitter === undefined) {
            this.emitter = new EventEmitter();
        }

        return this.emitter;
    }
}