/**
 * Provide URL's for proxy requests
 */
export default class UrlProvider {
    /**
     * @param {string} query
     * @param {int} page
     * @returns {string}
     */
    static getImages(query, page = 1) {
        return `/get_images?query=${query}&page=${page}`;
    }

    /**
     * @param {string} image
     * @returns {string}
     */
    static getInfo(image) {
        return '/get_info?image=' + image;
    }

    /**
     * @param {string} query
     * @returns {string}
     */
    static proxyQuery(query) {
        return '/proxy_query?query=' + query;
    }

    /**
     * @param {String} dockerfile
     * @returns {string}
     */
    static download(dockerfile) {
        return '/download?dockerfile=' + encodeURIComponent(dockerfile);
    }
}