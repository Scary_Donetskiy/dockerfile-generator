#!/usr/local/bin/python3
# -*- coding: utf-8 -*-
import cherrypy
from cherrypy.test import helper
from main import ProxyServer


class ProxyServerTest(helper.CPWebCase):
    def setup_server():
        cherrypy.tree.mount(ProxyServer())
    setup_server = staticmethod(setup_server)
    
    def test_open_index_page(self):
        self.getPage('/')
        self.assertStatus('200 OK')
        
    def test_get_images(self):
        self.getPage('/get_images?query=ubuntu')
        self.assertStatus('200 OK')
        
    def test_get_info(self):
        self.getPage('/get_info?image=ubuntu')
        self.assertStatus('200 OK')